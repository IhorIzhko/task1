def direction(facing, turn):
    directions = ('N','NE', 'E', 'SE', 'S', 'SW', 'W', 'NW')
    index = int(directions.index(facing) + turn / 45) % len(directions)
    return directions[index]
